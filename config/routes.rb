Rails.application.routes.draw do
  get 'users/new'
  resources :users
  root 'static_pages#home'
  get  '/statistics',    to: 'static_pages#statistics'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'
end
